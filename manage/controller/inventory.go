package controller

import (
	"database/sql"
	"encoding/json"
	"flower/model"
	"flower/utils/httpResp"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func Addflower(w http.ResponseWriter, r *http.Request) {
	var flow model.Flower

	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&flow); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")

		return
	}

	defer r.Body.Close()
	saveErr := flow.Create()
	if saveErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, saveErr.Error())
		return
	}

	httpResp.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "flower added"})

}

func GetFlow(w http.ResponseWriter, r *http.Request) {
	fid := mux.Vars(r)["fid"]

	old_FId, Fid_err := getFlowId(fid)

	if Fid_err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, Fid_err.Error())
	}

	f := model.Flower{FlowerId: old_FId}

	err_Flow := f.Read()

	if err_Flow != nil {

		switch err_Flow {

		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "flower not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, err_Flow.Error())
		}
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, f)

}

func getFlowId(userFid string) (int64, error) {
	Fid, err := strconv.ParseInt(userFid, 10, 64)

	if err != nil {
		return 0, err
	}
	return Fid, nil
}
func UpdateFlow(w http.ResponseWriter, r *http.Request) {
	fid := mux.Vars(r)["fid"]
	FID, err := strconv.ParseInt(fid, 10, 64)
	if err != nil {
		fmt.Println("Error in  converting string to integer", err)
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	var flower model.Flower
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&flower)
	if err != nil {
		fmt.Print("error in decoding the request")
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
	}

	UpdateError := flower.UPDATE(FID)
	if UpdateError != nil {
		switch UpdateError {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusBadRequest, "The particular flower is not registered in the database")
		default:
			fmt.Print("Error in updating the data")
			httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		}
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, "update succesfully")
}
