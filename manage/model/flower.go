package model

import "flower/dataStore/postgres"

type Meeting struct {
	MeetingID int64  `json:"meetingid"`
	Location  string `json:"location"`
	Date      int64  `json:"date"`
	Status    string `json:"status"`
}

const (
	queryInsertUser = "INSERT INTO Meeting(meetingid, location, date, status) VALUES($1,$2,$3,$4);"
	queryGetUser    = "SELECT meetingid,location,date,status FROM Meeting where meetingid=$1;"
	queryUpdate     = "UPDATE meeting SET meetingid=$1, location=$2, date=$3, status=$4 WHERE meetingid=$5 RETURNING meetingid"
	queryDeleteUser = "DELETE FROM meeting where meetingid=$1"
)

func (m *Meeting) Create() error {
	_, err := postgres.Db.Exec(queryInsertUser, m.MeetingID, m.Location, m.Date, m.Status)
	return err
}

func (m *Meeting) Read() error {
	return postgres.Db.QueryRow(queryGetUser,
		m.MeetingID).Scan(&m.MeetingID, &m.Location, &m.Date, &m.Status)
}

func (m *Meeting) Update(oldID int64) error {
	err := postgres.Db.QueryRow(queryUpdate, m.MeetingID, m.Location, m.Date, m.Status, oldID).Scan(&m.MeetingID)
	return err
}

func (m *Meeting) Delete() error {
	if _, err := postgres.Db.Exec(queryDeleteUser, m.MeetingID); err != nil {
		return err
	}
	return nil
}
func GetAllmets() ([]Meeting, error) {
	rows, getErr := postgres.Db.Query("SELECT * from meeting;")
	if getErr != nil {
		return nil, getErr
	}

	meeting := []Meeting{}

	for rows.Next() {
		var m Meeting
		dbErr := rows.Scan(&m.MeetingID, &m.Location, &m.Date, &m.Status)
		if dbErr != nil {
			return nil, dbErr
		}

		meeting = append(meeting, m)
	}
	rows.Close()
	return meeting, nil
}
