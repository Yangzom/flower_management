window.onload = function (){
    fetch('/flower')
        .then(response => response.text())
        .then(data => showMeeting(data))
}

function addmeeting(){
    var data = {
        meetingid  :parseInt(document.getElementById('id').value),
        location  : document.getElementById('location').value,
        date  : parseInt( document.getElementById('price').value),
        status : document.getElementById('status').value
    }
    var meid = data.metid
    fetch('/flower', {
        method: "POST",
        body : JSON.stringify(data),
        headers: {"Content-type": "application/json; charset=UTF-8"}
    }).then(response1 => {
        if (response1.ok) {
            fetch(`/flower/${meid}`)
            .then(response2 => response2.text())
            .then(data => showMeeting(data))
        }else{
            throw new Error(response1.statusText)
        }
    }).catch (e => {
        alert(e)
    })

    resetform();
}

function showMeeting (info) {
    data = JSON.parse(info)
    newRow(data)
}

function newRow(meeting) {
    var table = document.getElementById("myTable");

    var row = table.insertRow(table.length);
     
    var td=[]
    for (i=0 ; i<table.rows[0].cells.length; i++) {
        td[i] = row.insertCell(i);
    }

    td[0].innerHTML = meeting.meetingid;
    td[1].innerHTML = meeting.location;
    td[2].innerHTML = meeting.date;
    td[3].innerHTML = meeting.status;
    td[4].innerHTML = '<input type="button" onclick="deletemeeting(this)"value="delete" id="button-1">';
    td[5].innerHTML = '<input type="button" onclick="updateflower(this)" value="update" id="button-2">';

}
function showMeeting (data){
    const meetings = JSON.parse(data)
    meetings.forEach(met =>{
        newRow(met)
    })
}
function resetform() {
    document.getElementById("id").value = "";
    document.getElementById("location").value = "";
    document.getElementById("price").value = "";
    document.getElementById("status").value = "";
}
var selectedRow = null;

function updateflower(r){
    selectedRow = r.parentElement.parentElement;
    console.log("above form data")

    document.getElementById("id").value = selectedRow.cells[0].innerHTML;
    document.getElementById("location").value = selectedRow.cells[1].innerHTML;
    document.getElementById("price").value = selectedRow.cells[2].innerHTML;
    document.getElementById("status").value = selectedRow.cells[3].innerHTML;

    console.log("above btn")
    var btn = document.getElementById("button-add");
    id = selectedRow.cells[0].innerHTML;
    if(btn){
        btn.innerHTML = "Update";
        btn.setAttribute("onclick", `update(${id})`)
    }
}
function getformData() {
    var formData = {
        meetingid  :parseInt(document.getElementById('id').value),
        location  : document.getElementById('location').value,
        date  :     parseInt(document.getElementById('price').value),
        status :    document.getElementById('status').value
    }
    return formData
}

function update(id){
    var newData = getformData()

    console.log(newData);
    fetch(`/flower/${id}`, {
        method: "PUT",
        body : JSON.stringify(newData),
        headers: {"Content-Type": "application/json; charset=UTF-8"}
    }).then(res => {
        if(res.ok){
            selectedRow.cells[0].innerHTML = newData.meetingid;
            selectedRow.cells[1].innerHTML = newData.location;
            selectedRow.cells[2].innerHTML = newData.date;
            selectedRow.cells[3].innerHTML = newData.status;

            var button = document.getElementById("button-add");
            button,innerHTML = "Add" ;
            button.setAttribute("onclick", "addmeeting()");
            selectedRow = null;

            resetform()
        }else{
            alert("server: up req error")
        }
    })

}

function deletemeeting(r){
    if(confirm('are you sure you want to Delete this?')) {
        selectedRow = r.parentElement.parentElement;
        id = selectedRow.cells[0].innerHTML;

        fetch('/flower/' + id, {
            method : "DELETE",
            headers : {"COntent-type": "application/json; charset=UTF-8"}
        });
        var rowIndex = selectedRow.rowIndex;
        if(rowIndex>0){
            document.getElementById("myTable").deleteRow(rowIndex);
        }
        selectedRow = null;       
    }
}
